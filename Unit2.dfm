object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 250
  ClientWidth = 373
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 64
    Top = 80
    Width = 36
    Height = 13
    Caption = 'Cliente '
  end
  object Label3: TLabel
    Left = 58
    Top = 107
    Width = 42
    Height = 13
    Caption = 'Telefone'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 373
    Height = 41
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 32
      Top = 16
      Width = 76
      Height = 13
      Caption = 'Buscar registro:'
    end
    object Edit1: TEdit
      Left = 114
      Top = 12
      Width = 231
      Height = 21
      TabOrder = 0
      TextHint = 'Digite o nome que deseja encontrar...'
      OnChange = Edit1Change
      OnKeyDown = Edit1KeyDown
    end
  end
  object edtCliente: TEdit
    Left = 114
    Top = 76
    Width = 231
    Height = 21
    TabOrder = 1
  end
  object edtTelefone: TEdit
    Left = 114
    Top = 103
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Victor\Desktop\PESSOAL\videos\EDIT_LIST\EDITLI' +
        'ST.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'Server=localhost'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 80
    Top = 48
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 48
    Top = 104
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 184
    Top = 104
  end
  object qryClientes: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select cli_id, cli_nome, cli_telefone from clientes')
    Left = 64
    Top = 184
    object qryClientesCLI_NOME: TStringField
      FieldName = 'CLI_NOME'
      Origin = 'CLI_NOME'
      Size = 100
    end
    object qryClientesCLI_TELEFONE: TStringField
      FieldName = 'CLI_TELEFONE'
      Origin = 'CLI_TELEFONE'
    end
    object qryClientesCLI_ID: TIntegerField
      FieldName = 'CLI_ID'
      Origin = 'CLI_ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
end
