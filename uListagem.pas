unit uListagem;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.StdCtrls;

type
  TfrmListagem = class(TForm)
    grdGrade: TDBGrid;
    procedure grdGradeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    pField : TField;
    pDataSource : TDataSource;

  end;

var
  frmListagem: TfrmListagem;

implementation

{$R *.dfm}

procedure TfrmListagem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Action      := caFree;
     frmListagem := nil;
end;

procedure TfrmListagem.grdGradeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key = VK_RETURN then
        if grdGrade.DataSource.DataSet.IsEmpty then
           ModalResult := mrCancel
        else
           ModalResult := mrOk;
     if Key = VK_ESCAPE then
        ModalResult := mrCancel;
end;

end.
