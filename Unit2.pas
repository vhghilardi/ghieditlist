{
 Este fonte foi desenvolvido por Victor Henrique Ghilardi.
 https://www.facebook.com/toy.developer
 https://toydeveloper.blogspot.com
 http://www.youtube.com/c/ToyGhilardi
 � de uso e reprodu��o livre, desde que seja mantido os direitos autorais!
}

unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB,
  FireDAC.Comp.Client, FireDAC.Comp.UI, FireDAC.Phys.IBBase,

  Data.DBXMySQL, Data.SqlExpr,
  Data.DBXFirebird, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.ExtCtrls
;

  {
  }


type
  TForm2 = class(TForm)
    FDConnection1: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    qryClientes: TFDQuery;
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtCliente: TEdit;
    edtTelefone: TEdit;
    qryClientesCLI_NOME: TStringField;
    qryClientesCLI_TELEFONE: TStringField;
    qryClientesCLI_ID: TIntegerField;
    procedure Edit1Change(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    function Carrega_Listagem(pLeft, pTop: Integer; pCampo_Select, pCampoReturn,
      pCampo_Where, pValor_Where, pTabela: String): String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

uses uListagem;

procedure TForm2.Edit1Change(Sender: TObject);
var
   pRect:TRect;
begin
     GetWindowRect(Edit1.Handle,pRect);
     Carrega_Listagem(pRect.left, (pRect.top + Edit1.Height), 'cli_nome || '' - '' || cli_telefone', 'cli_id', 'cli_nome', Edit1.Text, 'clientes');
     Edit1.SetFocus;
     Edit1.Tag                := 777;

     if Edit1.Text = '' then
     begin
          if Assigned(frmListagem) then
             frmListagem.Close;
     end;
end;

procedure TForm2.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (Key = VK_RETURN) then
     begin
          if qryClientes.Locate('cli_id', frmListagem.grdGrade.DataSource.DataSet.FieldByName('retorno').AsString, []) then
          begin
              edtCliente.Text  := qryClientesCLI_NOME.AsString;
              edtTelefone.Text := qryClientesCLI_TELEFONE.AsString;
              Edit1.Text       := '';
              frmListagem := NIL;// Close;
          end;
     end;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
     qryClientes.Open();
end;

function TForm2.Carrega_Listagem(pLeft, pTop : Integer; pCampo_Select, pCampoReturn, pCampo_Where, pValor_Where, pTabela : String) : String;
var
   vQuery : TFDQuery;
   vDataSource : TDataSource;
   vQtde_Reg : Integer;
begin
     // Cria os componentes de banco para montar o SQL e vincular Datasource.
     vQuery              := TFDQuery.Create(Application);
     vDataSource         := TDataSource.Create(Application);
     vDataSource.DataSet := vQuery;

     with vQuery do
     begin
          Connection := FDConnection1;
          SQL.Clear;
          SQL.Add('select ' + pCampo_Select + ' as coluna, ' + pCampoReturn + ' as retorno from ' + pTabela + ' where ' + pCampo_Where + ' like ''%' + pValor_Where + '%'' order by 1');
          Open();
          vQtde_Reg := RecordCount;
     end;

     if vQtde_Reg > 0 then
     begin
         if not Assigned(frmListagem) then
            frmListagem := TfrmListagem.Create(Application);
         frmListagem.grdGrade.DataSource           := vDataSource;
         frmListagem.grdGrade.Columns[0].FieldName := 'coluna';
         frmListagem.grdGrade.Columns[0].Width     := 450;
         frmListagem.Left                          := pLeft;
         frmListagem.Top                           := pTop;
         frmListagem.Show;
         Application.ProcessMessages;
     end;
end;


end.
